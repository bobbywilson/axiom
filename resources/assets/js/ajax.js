// Resident ajax calls
$(document).on('click', '.utilities', function () {
    $.ajax({
        url: '/resident/resources/utilities',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.rent-buy', function () {
    $.ajax({
        url: '/resident/resources/rent-buy',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.landlords', function () {
    $.ajax({
        url: '/resident/resources/landlords',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.rentals', function () {
    $.ajax({
        url: '/resident/resources/rentals',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.credit', function () {
    $.ajax({
        url: '/resident/resources/credit',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});

//Client ajax calls
$(document).on('click', '.professionalism', function () {
    $.ajax({
        url: '/client/total-package/professionalism',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.placement', function () {
    $.ajax({
        url: '/client/total-package/placement',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.maintenance', function () {
    $.ajax({
        url: '/client/total-package/maintenance',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.fees-charges', function () {
    $.ajax({
        url: '/client/total-package/fees-charges',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.reporting', function () {
    $.ajax({
        url: '/client/total-package/reporting',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.other-services', function () {
    $.ajax({
        url: '/client/total-package/other-services',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.why-invest', function () {
    $.ajax({
        url: '/client/resources/why-invest',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.buying-process', function () {
    $.ajax({
        url: '/client/resources/buying-process',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.profit', function () {
    $.ajax({
        url: '/client/resources/profit',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.management', function () {
    $.ajax({
        url: '/client/resources/management',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});$(document).on('click', '.contact', function () {
    $.ajax({
        url: '/client/resources/contact',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});