@extends('_layout')
@section('content')
    <div class="container-fluid">
        <div class="jumbotron" id="nested">
            <h2>{!! $page !!}</h2> <br />
            <div class="container" id="button-group-wrapper-client">
                <button type="button" class="btn btn-primary placement" id="placement">Placement</button>
                <button type="button" class="btn btn-primary professionalism" id="professionalism">Professional</button>
                <button type="button" class="btn btn-primary maintenance" id="maintenance">Maintenance</button>
                <button type="button" class="btn btn-primary reporting" id="reporting">Reporting</button>
                <button type="button" class="btn btn-primary fees-charges" id="fees-charges">Fees & Charges</button>
                <button type="button" class="btn btn-primary other-services" id="other-services">Other Services</button>
            </div>
            <br /><br />
            <div class="jumbotron" id="resources">
                <h3>Full Residential Property Management Package</h3><br />
                <p class="text-justify">Here is what is included in our Full Residential Property Management Package. Click the buttons above to see each section.</p>
            </div>
        </div>
    </div>
@endsection