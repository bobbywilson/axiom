@extends('_layout')
@section('content')
    <div class="container-fluid">
        <div class="jumbotron" id="nested">
            <h2>{!! $page !!}</h2> <br />
            <div class="container" id="button-group-wrapper-client">
                <button type="button" class="btn btn-primary why-invest" id="why-invest">Why Invest</button>
                <button type="button" class="btn btn-primary buying-process" id="buying-process">Buying Process</button>
                <button type="button" class="btn btn-primary profit" id="profit">Maximize Profit</button>
                <button type="button" class="btn btn-primary management" id="management">Management</button>
                <button type="button" class="btn btn-primary contact" id="contact">Contact Us</button>
            </div>
            <br /><br />
            <div class="jumbotron" id="resources">
                <p class="text-justify">Select one of the articles listed above for helpful hints and timely topics related to real estate. No matter whether you're buying, selling or renting there's an article here for you.  We hope you find them very informative.</p><br />
            </div>
        </div>
    </div>
@endsection