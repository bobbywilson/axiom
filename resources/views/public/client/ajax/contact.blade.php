<h2>Contact Us</h2><br/>
<p class="text-justify">For information regarding our Property Management Services or
    one of the many Rental Properties that Axiom Property Management currently manages,
    you can contact us at {!! Config::get('app.BUSINESS_PHONE') !!} or send us a email to
    {!! Config::get('app.BUSINESS_EMAIL') !!}.<br/><br/> If you are a resident or an owner
    and would like to submit a Service Request you can reach us at
    {!! Config::get('app.BUSINESS_MAINTENANCE_EMAIL') !!}. <br/><br/>
    If you are a resident experiencing an after hours Maintenance Emergency please call
    {!! Config::get('app.BUSINESS_MAINTENANCE_AFTER_HOURS') !!}. </p><br/>
<hr><br/>
<p class="text-justify contact-link">For additional ways to contact us, or send us a comment or question,
    please click <a href="{{ url('/contact') }}" id="contact-link"> contact us.</a></p>
</p>