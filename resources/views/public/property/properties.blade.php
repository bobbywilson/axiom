@extends('_layout')
@section('content')
    <div class="container-fluid">
        <div class="jumbotron" id="nested">
            <h2>{!! $page !!}</h2> <br/>
            @foreach ($property as $item)
                <div class="row" id="property-box"><br/>
                    <div class="col-md-6" id="property-image"><br/>
                        <div id="property-carousel-{{ $item->id }}" class="carousel slide" data-ride="carousel"
                             data-interval="7000">
                            <!-- Indicators -->
                            <ol class="carousel-indicators" id="bullet-indicators">
                                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                                @for($indicator = 1; $indicator < count($images); $indicator++)
                                    <li data-target="#carousel" data-slide-to="{{ $indicator }}"></li>
                                @endfor
                            </ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="{{ $active_image }}" class="center-image">
                                    <div class="carousel-caption" id="bullet-indicators">
                                        <h4>{{ $item->availability }}</h4>
                                    </div>
                                </div>
                                @foreach (array_slice($images, 1) as $image)
                                    <div class="item">
                                        <img src="{{ $image }}" class="center-image">
                                        <div class="carousel-caption" id="bullet-indicators">
                                            <h4>{{ $item->availability }}</h4>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#property-carousel-{{ $item->id }}" role="button" data-slide="prev"
                               id="left">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#property-carousel-{{ $item->id }}" role="button"
                               data-slide="next"
                               id="right">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <br/>
                        <p class="text-justify" id="tour-button"><span class="glyphicon glyphicon-calendar"></span>
                            <a href="{{ url('/property/tours') }}"> Schedule a Property Tour</a>
                        </p>
                    </div>
                    <br/>
                    <div class="col-md-6" id="property-address"><br/>
                        <div class="panel panel-default" id="property-table">
                            <div class="panel-heading">
                                <span class="text-justify text-info text-muted">{{ ucwords( strtolower($item->name)) }}</span>
                            </div>
                            <div class="panel-body" id="table-panel">
                                <div class="col-md-8" id="property-address">
                                    <p class="text-justify text-muted">
                                        <small>{!! Html::decode(ucwords( strtolower($item->street)) . ", <br/> " . ucwords( strtolower($item->city)) . ", " . ucwords($item->state) . ", " . ucwords( strtolower($item->zip))) !!}</small>
                                    </p>
                                </div>
                                <div class="col-md-4" id="property-address">
                                    <p class="text-justify text-muted">
                                        <small>
                                            <a href="" target="_blank" type="button" class="btn btn-primary" id="apply" name="submit">Apply</a></small>
                                    </p>
                                </div>
                            </div>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td id="indent" class="text-info text-uppercase small"><b>beds/baths</b></td>
                                    <td></td>
                                    <td class="text-right">{{ $item->beds . " bd / " . $item->beds . " ba" }}</td>
                                </tr>
                                <tr>
                                    <td id="indent" class="text-info text-uppercase small"><b>square feet</b></td>
                                    <td></td>
                                    <td class="text-right">{{ number_format($item->square_feet,0) }}</td>
                                </tr>
                                <tr>
                                    <td id="indent" class="text-info text-uppercase small"><b>rent</b></td>
                                    <td></td>
                                    <td class="text-right">{{ "$" . number_format($item->rent,0) . " / month" }}</td>
                                </tr>
                                <tr>
                                    <td id="indent" class="text-info text-uppercase small"><b>security deposit</b></td>
                                    <td></td>
                                    <td class="text-right">{{ "$" . number_format($item->deposit,0) }}</td>
                                </tr>
                                <tr>
                                    <td id="indent" class="text-info text-uppercase small"><b>application fee</b></td>
                                    <td></td>
                                    <td class="text-right">{{ "$" . number_format($item->fee,0) }}</td>
                                </tr>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-group" id="accordion11">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion11" href="#collapse11"><b
                                                    class="text-info text-uppercase small">description</b></a>
                                    </h4>
                                </div>
                                <div id="collapse11" class="panel-collapse collapse">
                                    <div class="panel-body" id="panel-body">
                                        <p class="text-justify text-uppercase">
                                            <small>
                                                {!! Html::decode(ucwords($item->description) . "</br>" . Config::get('app.EHO_BLACK')) !!}
                                            </small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion1" href="#collapse1"><b
                                                        class="text-info text-uppercase small">amenities</b></a>
                                        </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse">
                                        <div class="panel-body" id="panel-body">
                                            <p class="text-justify text-uppercase">
                                                <small>{{ $item->amenities }}</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br/>
            @endforeach
        </div>
    </div>
@endsection