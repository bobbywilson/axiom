@extends('_layout')
@section('content')
    <div class="container-fluid">
        <div class="jumbotron" id="nested">
            <h2>{!! $page !!}</h2>
            <h3 class="text-info">Renting a Home Should be Fast and Easy</h3> <br />
            <p class="text-justify">Getting started can be overwhelming and deter you from taking the first step. A property specialist can have you moving forward in the time you (or perhaps while you) drink a cup of coffee. The objectives of this first visit include:</p>
            <ul>
                <li class="text-justify">Tour properties of interest</li>
                <li class="text-justify">Review a sample agreement</li>
                <li class="text-justify">Check your credit</li>
            </ul>
            <p class="text-justify">This visit typically takes between five and ten minutes based upon the nature of your questions. Call or complete the information below to schedule a visit now.</p>
            <p class="text-justify">Our office is open {!! Config::get('app.BUSINESS_HOURS') !!}. For your convenience, we are available 7 days a week for showings by appointment. Our mailing address is, {!! Config::get('app.BUSINESS_NAME') !!}, {!! Config::get('app.BUSINESS_ADDRESS') !!}. You can also give us a call at  {!! Config::get('app.BUSINESS_PHONE') !!} or send a fax to {!! Config::get('app.BUSINESS_FAX') !!} .</p>
        </div>
    </div>
@endsection