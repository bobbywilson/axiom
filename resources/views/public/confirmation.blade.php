@extends('_layout')
@section('content')
    <div class="container-fluid">
        <div class="jumbotron" id="nested-contact">
            <h2>{!! $page !!}</h2>
            <h4> Thank you for your comment!</h3><p class='text-justify'><b>{!! $contact->first_name !!} {!! $contact->last_name !!}</b><br/> {{ Html::mailto($contact->email, $contact->email) }} <br/> </p></h4>
            <p class="text-justify text-muted">{!! nl2br($contact->comment) !!}</p>
            <p class="text-justify"> <b>Submitted: {!! date("F, d, Y", strtotime($contact->created_at)) . " at " . date("h:i A", strtotime($contact->created_at)) !!} </b></p>
        </div>
    </div>
@endsection