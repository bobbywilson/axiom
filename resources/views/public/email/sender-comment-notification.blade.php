@extends('_email-layout')
@section('content')
<div class="jumbotron" id="main">
    <div class="container-fluid">
        <div class="jumbotron" id="nested-contact">
            <h2>{!! $confirmation_notice !!}</h2>
            <h3 class='text-justify'>Hello, {!! $contact->first_name !!} {!! $contact->last_name !!},</h3>
            <br/>
            <p>Thank you for your comment. Someone from Axiom will follow up with you soon.</p>
            <p id="normal-justify-text">For information regarding our Property Management Services, or one of the many Rental Properties that Axiom Property Management currently manages, you can contact us at {!! Config::get('app.BUSINESS_PHONE') !!} or send us a email to {!! Config::get('app.BUSINESS_EMAIL') !!}.
            If you are a resident or an owner and would like to submit a Service Request, you can reach us at {!! Config::get('app.BUSINESS_MAINTENANCE_EMAIL') !!}.
            <span id="error-return-here"></span>If you are a resident experiencing an after hours Maintenance Emergency please call {!! Config::get('app.BUSINESS_MAINTENANCE_AFTER_HOURS') !!}.</p>
            <h3 class='text-justify'>
                Thank You,
                <br/>
                {!! Config::get('app.BUSINESS_NAME') !!}
            </h3>
            <br/><br/>
            <p class="text-justify"> <b>Submitted: {!! date("F, d, Y", strtotime($contact->created_at)) . " at " . date("h:i A", strtotime($contact->created_at)) !!} </b></p>
        </div>
    </div>
</div>
@endsection
