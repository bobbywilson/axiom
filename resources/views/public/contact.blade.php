@extends('_layout')
@section('content')
    <div class="container-fluid">
        <div class="jumbotron" id="nested-contact">
            <h2>{!! $page !!}</h2> <br />
            <p id="normal-justify-text">For information regarding our Property Management Services, or one of the many Rental Properties that Axiom Property Management currently manages, you can contact us at {!! Config::get('app.BUSINESS_PHONE') !!} or send us a email to {!! Config::get('app.BUSINESS_EMAIL') !!}.
                If you are a resident or an owner and would like to submit a Service Request, you can reach us at {!! Config::get('app.BUSINESS_MAINTENANCE_EMAIL') !!}.
                <span id="error-return-here"></span>If you are a resident experiencing an after hours Maintenance Emergency please call {!! Config::get('app.BUSINESS_MAINTENANCE_AFTER_HOURS') !!}.</p> <br />
            <div class="bg-danger text-justify text-danger" id="client-errors-box"><p class="text-justify text-danger"></p></div>
            <br />
            @if (count($error_message))
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="text-danger">Your request was not submitted because of one or more invalid fields:</h4>
                        @foreach ($error_message as $error)
                            <p><strong>{{ $error }}</strong></p>
                        @endforeach
                    </div>
                </div>
            @endif
            <h5 class="text-right">Fields marked with a "<span class="required"> &bull; </span>" are required.</h5>
            <div class="row">
                <div class="col-md-5" id="col-md-left">
                    <a href="https://www.google.com/maps/place/2453+N+Delaware+St,+Indianapolis,+IN+46205/@39.802718,-86.152734,17z/data=!3m1!4b1!4m2!3m1!1s0x886b51184e2e5ccb:0x94ff3fef7ff5f2ce" target="_blank">
                        <div class="col-md-12" id="map-canvas">
                        </div>
                    </a>
                    <hr>
                    <span class="small-text phone-numbers-col"><p class="text-justify"><span class="glyphicon glyphicon-home"></span> <b>{!! Config::get('app.BUSINESS_NAME') !!}</b><br />{!! Config::get('app.BUSINESS_ADDRESS') !!}<br />{!! Config::get('app.BUSINESS_HOURS') !!}</p>
 						<hr>
   						<p class="text-justify"><span class="glyphicon glyphicon-info-sign"></span> Main Office Information<br /> {!! Config::get('app.BUSINESS_PHONE') !!} (main office)<br />{!! Config::get('app.BUSINESS_FAX') !!} (main fax)<br />{!! Config::get('app.BUSINESS_EMAIL') !!}</p>
   						<hr>
      					<span class="glyphicon glyphicon-user"></span> {!! Config::get('app.CEO_NAME') !!} <br />  <span class="glyphicon glyphicon-user" id="spacer"></span> {!! Config::get('app.CEO_TITLE') !!} <br /> {!! Config::get('app.CEO_PHONE') !!} <br /> {!! Config::get('app.CEO_EMAIL') !!} <br />
      					<hr>
                        <p class="text-justify"> <span class="glyphicon glyphicon-user"></span> {!! Config::get('app.VICE_NAME') !!} <br /> <span class="glyphicon glyphicon-user" id="spacer"></span> {!! Config::get('app.VICE_TITLE') !!} <br />  {!! Config::get('app.VICE_PHONE') !!}  <br />  {!! Config::get('app.VICE_EMAIL') !!}  <br /></p>
      					<hr>
      					<p class="text-justify">{!! Config::get('app.BUSINESS_MAINTENANCE') !!}<br />{!! Config::get('app.BUSINESS_MAINTENANCE_EMERGENCY') !!}<br />{!! Config::get('app.BUSINESS_MAINTENANCE_AFTER_HOURS') !!} (emergency)<br />{!! Config::get('app.BUSINESS_MAINTENANCE_EMAIL') !!}<br /></p>
						</span>
                </div>
                <div class="col-md-7" id="col-md-center">
                    {!! Form::open(array('url' => 'contact', 'role' => 'form-inline', 'name' => 'contact_form', 'id' => 'contact_form')) !!}
                        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                            <div class="col-xs-12" id="col-xs">
                                {!! Html::decode(Form::label('first_name', 'First Name<span class="required"> &bull;</span>', $errors->has('first_name') ? ['class' => 'control-label noError'] : ['class' => 'control-label'])) !!}
                                {!! Form::text('first_name', null, $errors->has('first_name') ? ['class' => 'form-control hasError', 'id' => "first_name", 'name' => 'first_name', 'placeholder' => 'i.e. John', 'tabindex' => 1] :
                                ['class' => 'form-control', 'id' => "first_name", 'name' => 'first_name', 'placeholder' => 'i.e. John', 'tabindex' => 1]) !!}
                            </div>
                            {!! $errors->has('first_name') ? '<p class="col-sm-8 text-danger" id="error-present"><b>' . $errors->first('first_name') . '</b></p>' : '' !!}
                        </div>
                        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                            <div class="col-xs-12" id="col-xs">
                                {!! Html::decode(Form::label('last_name', 'Last Name<span class="required"> &bull;</span>', $errors->has('last_name') ? ['class' => 'control-label noError'] : ['class' => 'control-label'])) !!}
                                {!! Form::text('last_name', null, $errors->has('last_name') ? ['class' => 'form-control hasError', 'id' => "last_name", 'name' => 'last_name', 'placeholder' => 'i.e. Smith', 'tabindex' => 1] :
                                ['class' => 'form-control', 'id' => "last_name", 'name' => 'last_name', 'placeholder' => 'i.e. Smith', 'tabindex' => 2]) !!}
                            </div>
                            {!! $errors->has('last_name') ? '<p class="col-sm-8 text-danger" id="error-present"><b>' . $errors->first('last_name') . '</b></p>' : '' !!}
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <div class="col-xs-12" id="col-xs">
                                {!! Html::decode(Form::label('email', 'Email<span class="required"> &bull;</span>', $errors->has('email') ? ['class' => 'control-label noError'] : ['class' => 'control-label'])) !!}
                                {!! Form::email('email', null, $errors->has('email') ? ['class' => 'form-control hasError', 'id' => "email", 'name' => 'email', 'placeholder' => 'john.smith@example.com', 'tabindex' => 1] :
                                ['class' => 'form-control', 'id' => "email", 'name' => 'email', 'placeholder' => 'john.smith@example.com', 'tabindex' => 3]) !!}
                            </div>
                            {!! $errors->has('email') ? '<p class="col-sm-8 text-danger" id="error-present"><b>' . $errors->first('email') . '</b></p>' : '' !!}
                        </div>
                        <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                            <div class="col-xs-12" id="col-xs">
                                {!! Html::decode(Form::label('comment', 'Comment<span class="required"> &bull;</span>', $errors->has('comment') ? ['class' => 'control-label noError'] : ['class' => 'control-label'])) !!}
                                {!! Form::textarea('comment', null, $errors->has('comment') ? ['class' => 'form-control hasError', 'id' => "comment", 'name' => 'comment', 'placeholder' => 'Please enter a comment.', 'tabindex' => 1] :
                                ['class' => 'form-control', 'id' => "comment", 'name' => 'comment', 'placeholder' => 'Please enter a comment.', 'tabindex' => 4]) !!}
                            </div>
                            {!! $errors->has('comment') ? '<p class="col-sm-8 text-danger" id="error-present"><b>' . $errors->first('comment') . '</b></p>' : '' !!}
                        </div>
                        <div class="form-group" id="submit-button-box">
                            <div class="col-xs-12" id="col-xs">
                                <label class="sr-only" for="submit">Submit</label>
                                <button type="submit" class="btn btn-default btn-lg" id="submit" name="submit" onsubmit="validateForm()" tabindex="5">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection