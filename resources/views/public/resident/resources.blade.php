@extends('_layout')
@section('content')
    <div class="container-fluid">
        <div class="jumbotron" id="nested">
            <h2>{!! $page !!}</h2> <br />
            <div class="container" id="button-group-wrapper">
                <button type="button" class="btn btn-primary utilities" id="utilities">Utilities</button>
                <button type="button" class="btn btn-primary rent-buy" id="rent-buy">Rent or Buy</button>
                <button type="button" class="btn btn-primary landlords" id="landlords">The Landlord</button>
                <button type="button" class="btn btn-primary rentals" id="rentals">Rentals</button>
                <button type="button" class="btn btn-primary credit" id="credit">Credit Score</button>
            </div>
            <br /><br />
            <div class="jumbotron" id="resources">
                <p class="text-justify">Select one of the articles listed above for helpful hints and timely topics related to real estate. No matter whether you're buying, selling or renting there's an article here for you.  We hope you find them very informative.</p><br />
            </div>
        </div>
    </div>
@endsection