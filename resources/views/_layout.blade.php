
<!DOCTYPE html>
<html lang="en">
<head>
    <title> {!! Config::get('app.BUSINESS_NAME') !!} :: {{ $page }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="{{ elixir('js/all.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3uds6NngEcOJmE1WBhAJK9P8OfGoWYfI&callback=initialize" async defer></script>
    <link rel="stylesheet" href="{{ elixir('css/all.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body onload="window.setTimeout('TimeoutRedirect()', 1000000);" onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="noBack();">
<nav class="navbar navbar-inverse" id="navbar-wrapper" style="border-bottom: 12px solid #000;">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/') }}"><span class="glyphicon glyphicon-home" id="home-icon" ></span></a>
        </div>
        <div class="nav-buttons" id="header-buttons">
            <ul class="nav navbar-nav">
                <li {!! $home_status !!} ><a href="{{ url('/') }}">Home</a></li>
                <li {!! $about_status !!} ><a href="{{ url('/about') }}">About Us</a></li>
                <li {!! $property_status !!} ><a class="dropdown-toggle" data-toggle="dropdown" href="{{ url('/property/property') }}">Available Properties</a>
                    <ul class="dropdown-menu" id="properties">
                        <li class="dropdown" id="dropdown"><a href="{{ url('/property/property-list') }}"><span class="glyphicon glyphicon-search"></span> Browse Rental Property</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="{{ url('/property/introduction') }}"><span class="glyphicon glyphicon-book"></span> Read Rental Introduction</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="{{ url('/property/tours') }}"><span class="glyphicon glyphicon-calendar"></span> Schedule a Property Tour</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="{{ url('/property/application') }}"><span class="glyphicon glyphicon-list-alt"></span> Complete Rental Application</a></li>
                    </ul>
                </li>
                <li {!! $resident_status !!} ><a class="dropdown-toggle" data-toggle="dropdown" href="{{ url('/resident/resident') }}">Residents</a>
                    <ul class="dropdown-menu" id="residents">
                        <li class="dropdown" id="dropdown"><a href="{{ url('/resident/introduction') }}"><span class="glyphicon glyphicon-book"></span> Read Resident Introduction</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="{{ url('/resident/handbook') }}"><span class="glyphicon glyphicon-info-sign"></span> Read Resident Handbook</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="{{ url('/resident/resources') }}"><span class="glyphicon glyphicon-check"></span> View Resident Resources</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="{{ url('/resident/faq') }}"><span class="glyphicon glyphicon-question-sign"></span> Read Resident FAQ</a></li>
                        <!--<li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/residents/resident_login.php"><span class="glyphicon glyphicon-log-in"></span> Login as a Resident</a></li>-->
                    </ul>
                </li>
                <li {!! $client_status !!} ><a class="dropdown-toggle" data-toggle="dropdown" href="{{ url('/client/client') }}">Property Management</a>
                    <ul class="dropdown-menu" id="clients">
                        <li class="dropdown" id="dropdown"><a href="{{ url('/client/introduction') }}"><span class="glyphicon glyphicon-book"></span> Read Client Introduction</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="{{ url('/client/property-management') }}"><span class="glyphicon glyphicon-link"></span> Why Get a Property Manager</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="{{ url('/client/total-package') }}"><span class="glyphicon glyphicon-tag"></span> Total Real Estate Package</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="{{ url('/client/resources') }}"><span class="glyphicon glyphicon-check"></span> View Client Resources</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="{{ url('/client/faq') }}"><span class="glyphicon glyphicon-question-sign"></span> Read Client FAQ</a></li>
                        {{--<li role="presentation" class="divider"></li>--}}
                        {{--<li class="dropdown" id="dropdown"><a href="{{ url('/admin/login') }}"><span class="glyphicon glyphicon-log-in"></span> Login as a Client</a></li>--}}
                    </ul>
                </li>
                <li {!! $contact_status !!} ><a href="{{ url('/contact') }}">Contact</a></li>
            </ul>
            <form method="POST" role="form-inline">
                <ul class="nav navbar-nav navbar-right" id="header-contact">
                    <li><a href="{{ url('/contact') }}">{!! Config::get('app.BUSINESS_PHONE') !!}</a></li>
                    <li><a href="{{ url('/contact') }}">{!! Config::get('app.BUSINESS_FAX') !!}</a></li>
                    <li><a href="{{ url('/admin/login') }}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </form>
        </div>
    </div>
    <div class="navbar-header navbar-header-mobile" id="navbar-header-mobile">
        <ul class="nav navbar-nav">
            <li><a class="dropdown-toggle" data-toggle="dropdown" href="{{ url('/property/property-list') }}"><i class="fa fa-bars fa-lg"></i></a>
                <ul class="dropdown-menu" id="iphone-menu">
                    <li class="dropdown" id="dropdown"><a href="{{ url('/about') }}"><span class="glyphicon glyphicon-question-sign"></span> About Us</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/property/property-list') }}"><span class="glyphicon glyphicon-search"></span> Browse Rental Property</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/property/introduction') }}"><span class="glyphicon glyphssicon-book"></span> Read Rental Introduction</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/property/tours') }}"><span class="glyphicon glyphicon-calendar"></span> Schedule a Property Tour</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/property/introduction') }}"><span class="glyphicon glyphicon-list-alt"></span> Complete Rental Application</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/resident/introduction') }}"><span class="glyphicon glyphicon-book"></span> Read Resident Introduction</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/resident/handbook') }}"><span class="glyphicon glyphicon-info-sign"></span> Read Resident Handbook</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/resident/resources') }}"><span class="glyphicon glyphicon-check"></span> View Resident Resources</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/resident/faq') }}"><span class="glyphicon glyphicon-question-sign"></span> Read Resident FAQ</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/client/introduction') }}"><span class="glyphicon glyphicon-book"></span> Read Client Introduction</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/client/property-management') }}"><span class="glyphicon glyphicon-link"></span> Why Get a Property Manager</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/client/total-package') }}"><span class="glyphicon glyphicon-tag"></span> Total Real Estate Package</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/client/resources') }}"><span class="glyphicon glyphicon-check"></span> View Client Resources</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/client/faq') }}p"><span class="glyphicon glyphicon-question-sign"></span> Read Client FAQ</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="{{ url('/contact') }}"><span class="glyphicon glyphicon-envelope"></span> Contact</a></li>
                    <span class="not-showing"><li role="presentation" class="divider" id="divider"></li>
           					<li class="dropdown" id="dropdown"><a href="{{ url('/admin/login') }}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li></span>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li><a href="{{ url('/admin/login') }}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<nav class="navbar navbar-inverse" id="logo-wrapper">
    <div class="container-fluid">
        <div class="navbar-header logo">
            <a class="navbar-brand" href="{{ url('/') }}"><img class="img-responsive" src="/images/id5.jpg" alt="" id=""></a>
        </div>
    </div>
</nav>
<div id="carousel-wrapper">
    <div id="carousel" data-interval="7000" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators" id="bullet-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
            <li data-target="#carousel" data-slide-to="3"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="/images/id1.jpg" alt="" class="img-full-width">
            </div>
            <div class="item">
                <img src="/images/id2.jpg" alt="" class="img-full-width">
            </div>
            <div class="item">
                <img src="/images/id3.jpg" alt="" class="img-full-width">
            </div>
            <div class="item">
                <img src="/images/id4.jpg" alt="" class="img-full-width">
            </div>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" id="iphone" href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" id="iphone" href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<div class="jumbotron" id="main">
    @yield('content')
</div>
<nav class="navbar navbar-inverse" id="navbar-footer-wrapper">
    <div class="container-fluid" id="footer-container">
        <div class="navbar-footer">
            <p class="text-center">{!! Config::get('app.COPYRIGHT') !!}, {!! date("Y", strtotime(Config::get('app.CURRENT_YEAR'))) !!} <span id="raised"><img class="eho-img" src="/images/eho_footer.jpg"></span></p>
        </div>
    </div>
</nav>
</body>
</html>