<!DOCTYPE html>
<html lang="en">
<head>
    <title> {!! Config::get('app.BUSINESS_NAME') !!} :: {{ $page }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="cache-control" content="max-age=0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
    <meta http-equiv="pragma" content="no-cache">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="{{ elixir('js/all.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3uds6NngEcOJmE1WBhAJK9P8OfGoWYfI&callback=initialize" async defer></script>
    <style>
        body	{
            background-color: #EEE;
            padding: 0 !important;
        }
        /*.email-confirmation-link {*/
            /*color: blue !important;*/
        /*}*/
        .submit-date {
            color: #444 !important;
        }
        #logo-wrapper	{
            border-radius: 0px;
            border-bottom: 2px solid #000;
            border-left: 0px solid #000;
            border-right: 0px solid #000;
            background-color: #FFF;
            padding-top: 10px !important;
            position: fixed;
            top: 50px !important;
            width: 100%;
            height: 100px !important;
            z-index: 3 !important;
            opacity: 0.5 !important;
        }
        #logo-wrapper a:link, a:visited	{
            border-radius: 0px;
            color: #FFF !important;
            z-index: 3 !important;
        }
        #navbar-wrapper	{
            border-radius: 0px;
            border-top: 0px solid #000 !important;
            border-bottom: 4px solid #000;
            position: fixed;
            top: 0px !important;
            width: 100%;
            height: 7.5% !important;
            z-index: 5 !important;
        }
        #navbar-wrapper a:link, a:visited	{
            border-radius: 0px;
            color: #FFF !important;
            z-index: 5 !important;
        }
        .carousel-inner > .item > img, .carousel-inner > .item > a > img	{
            width: 50%;
            margin: auto;
        }
        #carousel	{
            margin-top: 50px;
            border-top: 0px solid #000;
            border-bottom: 2px solid #333;
        }
        #carousel-wrapper	{
            position: relative;
            left: 0px !important;
            right: 0px !important;
            background-color: #FFF !important;
            width: 100%;
        }
        .img-full-width	{
            width: 100% !important;
            z-index: -1 !important;
        }
        .carousel {
            height: auto !important;
            overflow: hidden;
        }
        .carousel .item {
            -webkit-transition: opacity 1s;
            -moz-transition: opacity 1s;
            -ms-transition: opacity 1s;
            -o-transition: opacity 1s;
            transition: opacity 1s;
        }
        .carousel .active.left, .carousel .active.right {
            left: 0;
            opacity: .7;
            z-index: 1;
        }
        .carousel .next, .carousel .prev {
            left: 0;
            opacity: .7;
            z-index: -2;
        }
        #navbar-footer-wrapper	{
            border-radius: 0px;
            position: relative;
            bottom: 0px !important;
            margin-bottom: 0px !important;
            color: #FFF !important;
            width: 100%;
            height: 100px;
        }
        #navbar-footer-wrapper a:link, a:visited	{
            border-radius: 0px;
            color: #FFF !important;
        }
        #footer-container	{
            padding-top: 2%;
            border: 0px solid #FFF;
        }
        @media screen and (-webkit-min-device-pixel-ratio:0) {
            #navbar-footer-wrapper  {
                margin-bottom: -40px !important;
            }
        }
        .text-justify	{
            font-size: 120% !important;
            color: #000 !important;
        }
        #main	{
            margin-bottom: 4px;
            padding: 0px !important;
        }
        .jumbotron p {
            font-size: 14px !important;
            color: #000 !important;
        }
        #active a	{
            color: #333 !important;
            cursor: pointer !important;
        }
        .contact-link a:active, .contact-link a:visited{
            color: #428BCA !important;
        }
        @media screen and (max-width: 1175px) {
            #navbar-wrapper	{
                position: absolute;
                top: 0px !important;
                right: 0px !important;
                width: 100% !important;
            }
            #logo-wrapper	{
                top: 42px !important;
                right: 0px !important;
                height: 90px !important;
                position: absolute;
                width: 100% !important;
            }
            #iphone-menu a:hover	{
                background-color: #000 !important;
                font-weight: bold;
            }
        }
        @media screen and (max-width: 768px) {
            p {
                text-align: left !important;
            }
            .logo {
                border: 0px solid #c00;
                margin-left: auto !important;
                margin-right: auto !important;
                width: 200px !important;
            }
            .fa .fa-bars .fa-lg {
                font-size: 10px !important;
            }
            #navbar-wrapper	{
                position: absolute;
                top: 0px !important;
                right: 0px !important;
                width: 100% !important;
            }
            #logo-wrapper	{
                top: 42px !important;
                right: 0px !important;
                height: 90px !important;
                position: absolute;
                width: 100% !important;
            }
            #iphone-menu a:hover	{
                background-color: #000 !important;
                font-weight: bold;
            }
            #nested-contact	{
                width: 100%;
                margin-left: auto;
                margin-right: auto;
                border: 0px solid #333;
                border-radius: 5px;
            }
           .iphone-blue-link {
               color: blue !important;
           }
            #carousel	{
                margin-top: 132px;
                width: 100% !important;
                right: 0px !important;
            }
            .button-bottom a:active, a:visited, a:link	{
                color: #fff !important;
            }
            .jumbotron {
                padding: 5px !important;
                width: 100% !important;
            }
            .jumbotron h1, h2, h3, h4, h5, h6 {
                text-align: left !important;
            }
            .text-center-footer {
                text-align: center !important;
            }
            #navbar-footer-wrapper p {
                margin-top: 25px !important;
                font-size: 90% !important;
            }
            .eho-img {
                height: 25px !important;
                width: auto !important;
            }
            .jumbotron a {
                color: #428BCA !important;
            }
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body onload="window.setTimeout('TimeoutRedirect()', 1000000);" onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="noBack();">
<nav class="navbar navbar-inverse" id="navbar-wrapper" style="border-bottom: 12px solid #000;">
    <div class="container-fluid">
        <div class="navbar-header"></div>
    </div>
</nav>
<nav class="navbar navbar-inverse" id="logo-wrapper">
    <div class="container-fluid">
        <div class="navbar-header logo">
            <a class="navbar-brand" href="{{ url('/') }}"><img class="img-responsive" src="http://bobbywwilson.com/axiom/assets/images/id5.jpg" alt="" id=""></a>
        </div>
    </div>
</nav>
<div id="carousel-wrapper">
    <div id="carousel" data-interval="7000" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="http://bobbywwilson.com/axiom/assets/images/id1.jpg" alt="" class="img-full-width">
            </div>
            <div class="item">
                <img src="http://bobbywwilson.com/axiom/assets/images/id2.jpg" alt="" class="img-full-width">
            </div>
            <div class="item">
                <img src="http://bobbywwilson.com/axiom/assets/images/id3.jpg" alt="" class="img-full-width">
            </div>
            <div class="item">
                <img src="http://bobbywwilson.com/axiom/assets/images/id4.jpg" alt="" class="img-full-width">
            </div>
        </div>
    </div>
</div>
    @yield('content')
<nav class="navbar navbar-inverse" id="navbar-footer-wrapper">
    <div class="container-fluid" id="footer-container">
        <div class="navbar-footer">
            <p class="text-center-footer">{!! Config::get('app.COPYRIGHT') !!}, {!! date("Y", strtotime(Config::get('app.CURRENT_YEAR'))) !!} <span id="raised"><img class="eho-img" src="/images/eho_footer.jpg"></span></p>
        </div>
    </div>
</nav>
</body>
</html>


