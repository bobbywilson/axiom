<!DOCTYPE html>
<html lang="en">
<head>
    <title>Axiom Property Management LLC :: Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="cache-control" content="max-age=0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
    <meta http-equiv="pragma" content="no-cache">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <link rel="stylesheet" href="http://bobbywwilson.com/axiom/assets/css/index.css">
    <link rel="stylesheet" href="http://bobbywwilson.com/axiom/assets/css/responsive.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="http://bobbywwilson.com/axiom/assets/js/javascript.js"></script>
    <script type="text/javascript">
        function loadRentBuy()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/resident/rent_or_buy.txt",true);xmlhttp.send();}
        function loadRentals()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/resident/rentals.txt",true);xmlhttp.send();}
        function loadLandlord()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/resident/landlord.txt",true);xmlhttp.send();}
        function loadCredit()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/resident/credit_score.txt",true);xmlhttp.send();}
        function loadUtilities()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/resident/utilities.txt",true);xmlhttp.send();}
        function loadPlacement() {var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_package/placement.txt",true);xmlhttp.send();}
        function loadProfessional()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_package/professional.txt",true);xmlhttp.send();}
        function loadMaintenance()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_package/maintenance.txt",true);xmlhttp.send();}
        function loadReporting()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_package/reporting.txt",true);xmlhttp.send();}
        function loadFee()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_package/fee.txt",true);xmlhttp.send();}
        function loadOther()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");} xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_package/other.txt",true);xmlhttp.send();}
        function loadInvest()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_resources/why_invest.txt",true);xmlhttp.send();}
        function loadBuy()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_resources/buying_process.txt",true);xmlhttp.send();}
        function loadProfit() {var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_resources/profit.txt",true);xmlhttp.send();}
        function loadManagement()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_resources/management.txt",true);xmlhttp.send();}
        function loadContact()	{var xmlhttp; if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();} else
        {xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");} xmlhttp.onreadystatechange=function()	{
            if (xmlhttp.readyState==4 && xmlhttp.status == 200)	{document.getElementById("resources").innerHTML=xmlhttp.responseText;}}
            xmlhttp.open("GET","http://bobbywwilson.com/axiom/assets/ajax/client/client_resources/contact_us.txt",true);xmlhttp.send();}
    </script>
</head>
<body onload="window.setTimeout('TimeoutRedirect()', 1000000);" onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="noBack();">
<nav class="navbar navbar-inverse" id="navbar-wrapper" style="border-bottom: 12px solid #000;">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://bobbywwilson.com/axiom/index.php"><span class="glyphicon glyphicon-home" id="home-icon" ></span></a>
        </div>
        <div class="nav-buttons" id="header-buttons">
            <ul class="nav navbar-nav">
                <li class="active" ><a href="http://bobbywwilson.com/axiom/index.php">Home</a></li>
                <li  ><a href="http://bobbywwilson.com/axiom/about/index.php">About Us</a></li>
                <li ><a class="dropdown-toggle" data-toggle="dropdown" href="http://bobbywwilson.com/axiom/properties/index.php">Available Properties</a>
                    <ul class="dropdown-menu" id="properties">
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/properties/index.php"><span class="glyphicon glyphicon-search"></span> Browse Rental Property</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/properties/rental_introduction.php"><span class="glyphicon glyphicon-book"></span> Read Rental Introduction</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/properties/tour.php"><span class="glyphicon glyphicon-calendar"></span> Schedule a Property Tour</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/properties/rental_application.php"><span class="glyphicon glyphicon-list-alt"></span> Complete Rental Application</a></li>
                    </ul>
                </li>
                <li ><a class="dropdown-toggle" data-toggle="dropdown" href="http://bobbywwilson.com/axiom/residents/index.php">Residents</a>
                    <ul class="dropdown-menu" id="residents">
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/residents/resident_introduction.php"><span class="glyphicon glyphicon-book"></span> Read Resident Introduction</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/residents/resident_handbook.php"><span class="glyphicon glyphicon-info-sign"></span> Read Resident Handbook</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/residents/resident_resources.php"><span class="glyphicon glyphicon-check"></span> View Resident Resources</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/residents/facts_questions.php"><span class="glyphicon glyphicon-question-sign"></span> Read Resident FAQ</a></li>
                        <!--<li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/residents/resident_login.php"><span class="glyphicon glyphicon-log-in"></span> Login as a Resident</a></li>-->
                    </ul>
                </li>
                <li ><a class="dropdown-toggle" data-toggle="dropdown" href="http://bobbywwilson.com/axiom/management/index.php">Property Management</a>
                    <ul class="dropdown-menu" id="clients">
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/client_introduction.php"><span class="glyphicon glyphicon-book"></span> Read Client Introduction</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/why_manager.php"><span class="glyphicon glyphicon-link"></span> Why Get a Property Manager</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/real_package.php"><span class="glyphicon glyphicon-tag"></span> Total Real Estate Package</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/management_resources.php"><span class="glyphicon glyphicon-check"></span> View Client Resources</a></li>
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/facts_questions.php"><span class="glyphicon glyphicon-question-sign"></span> Read Client FAQ</a></li>
                        <!--<li role="presentation" class="divider"></li>
                        <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/management_login.php"><span class="glyphicon glyphicon-log-in"></span> Login as a Client</a></li>-->
                    </ul>
                </li>
                <li ><a href="http://bobbywwilson.com/axiom/contact/index.php">Contact Us</a></li>
            </ul>
            <form method="POST" role="form-inline">
                <ul class="nav navbar-nav navbar-right" id="header-contact">
                    <li> <a href="http://bobbywwilson.com/axiom/contact/index.php"><span class='glyphicon glyphicon-phone'></span> (317) 259-0200</a></li>
                    <li><a href="http://bobbywwilson.com/axiom/contact/index.php"><span class='glyphicon glyphicon-print'></span> (317) 259-4578</a></li>
                    <li><a href="http://bobbywwilson.com/axiom/administrator/index.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </form>
        </div>
    </div>
    <div class="navbar-header navbar-header-mobile" id="navbar-header-mobile">
        <ul class="nav navbar-nav">
            <li><a class="dropdown-toggle" data-toggle="dropdown" href="http://bobbywwilson.com/axiom/properties/index.php"><i class="fa fa-bars fa-lg"></i></span></a>
                <ul class="dropdown-menu" id="iphone-menu">
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/about/index.php"><span class="glyphicon glyphicon-question-sign"></span> About Us</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/properties/index.php"><span class="glyphicon glyphicon-search"></span> Browse Rental Property</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/properties/rental_introduction.php"><span class="glyphicon glyphicon-book"></span> Read Rental Introduction</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/properties/tour.php"><span class="glyphicon glyphicon-calendar"></span> Schedule a Property Tour</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/properties/rental_application.php"><span class="glyphicon glyphicon-list-alt"></span> Complete Rental Application</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/residents/resident_introduction.php"><span class="glyphicon glyphicon-book"></span> Read Resident Introduction</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/residents/resident_handbook.php"><span class="glyphicon glyphicon-info-sign"></span> Read Resident Handbook</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/residents/resident_resources.php"><span class="glyphicon glyphicon-check"></span> View Resident Resources</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/residents/facts_questions.php"><span class="glyphicon glyphicon-question-sign"></span> Read Resident FAQ</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/client_introduction.php"><span class="glyphicon glyphicon-book"></span> Read Client Introduction</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/why_manager.php"><span class="glyphicon glyphicon-link"></span> Why Get a Property Manager</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/real_package.php"><span class="glyphicon glyphicon-tag"></span> Total Real Estate Package</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/management_resources.php"><span class="glyphicon glyphicon-check"></span> View Client Resources</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/management/facts_questions.php"><span class="glyphicon glyphicon-question-sign"></span> Read Client FAQ</a></li>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/contact/index.php"><span class="glyphicon glyphicon-envelope"></span> Contact Us</a></li>
                    <span class="not-showing"><li role="presentation" class="divider" id="divider"></li>
           					<li class="dropdown" id="dropdown"><a href="http://bobbywwilson.com/axiom/administrator/index.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li></span>
                    <li role="presentation" class="divider" id="divider"></li>
                    <li><a href="http://bobbywwilson.com/axiom/administrator/index.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<nav class="navbar navbar-inverse" id="logo-wrapper">
    <div class="container-fluid">
        <div class="navbar-header logo">
            <a class="navbar-brand" href="http://bobbywwilson.com/axiom/index.php"><img class="img-responsive" src="http://bobbywwilson.com/axiom/assets/images/id5.jpg" alt="" id=""></a>
        </div>
    </div>
</nav>
<div id="carousel-wrapper">
    <div id="carousel" data-interval="7000" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators" id="bullet-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <li data-target="#carousel" data-slide-to="1"></li>
            <li data-target="#carousel" data-slide-to="2"></li>
            <li data-target="#carousel" data-slide-to="3"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="http://bobbywwilson.com/axiom/assets/images/id1.jpg" alt="" class="img-full-width">
            </div>
            <div class="item">
                <img src="http://bobbywwilson.com/axiom/assets/images/id2.jpg" alt="" class="img-full-width">
            </div>
            <div class="item">
                <img src="http://bobbywwilson.com/axiom/assets/images/id3.jpg" alt="" class="img-full-width">
            </div>
            <div class="item">
                <img src="http://bobbywwilson.com/axiom/assets/images/id4.jpg" alt="" class="img-full-width">
            </div>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" id="iphone" href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" id="iphone" href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<div class="jumbotron" id="main">