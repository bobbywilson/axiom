<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('street');
            $table->string('city');
            $table->string('state');
            $table->integer('zip');
            $table->integer('beds');
            $table->integer('baths');
            $table->integer('square_feet');
            $table->float('rent', 8, 2);
            $table->float('fee', 8, 2);
            $table->float('deposit', 8, 2);
            $table->text('description');
            $table->text('name');
            $table->text('amenities');
            $table->text('promotions');
            $table->string('availability');
            $table->json('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
