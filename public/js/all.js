// JavaScript Document Bobby Wilson
function initialize() {
    // Styles a map in night mode.
    var myLatLng = {lat: 39.802891, lng: -86.152788};
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: myLatLng,
        zoom: 16,
        title: 'Axiom Property Management LLC',
        scrollwheel: true,
        zoomControl: true,
        styles: [
            {elementType: 'geometry', stylers: [{color: '#222222'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#222222'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#c00000'}]},
            {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{color: '#ffffff'}]
            },
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{color: '#333333'}]
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{color: '#6b9a76'}]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#000000'}]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{color: '#212a37'}]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{color: '#9ca5b3'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#746855'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#1f2835'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{color: '#f3d19c'}]
            },
            {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{color: '#2f3948'}]
            },
            {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{color: '#d59563'}]
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{color: '#17263c'}]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#515c6d'}]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#17263c'}]
            }
        ]
    });
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Axiom Property Management, LLC'
    });
}
$('#collapse1').collapse("hide");
//Form Validation
jQuery(window).load(function () {
    var error_present = document.getElementById("error-present");
    if (jQuery(error_present, ":contains('required')").is(':visible')) {
        $('html, body').animate({ scrollTop: $('#error-return-here').offset().top}, 2000);
    }
});

function validateForm()		{
    var requiredFields = ["first_name", "last_name", "email", "comment"];
    var errorFields = [];
    var errorMessages = [];
    for (i = 0; i < requiredFields.length; i++)	{
        var formFieldValue = document.forms["contact_form"][requiredFields[i]].value;
        if (checkEmpty(formFieldValue))		{
            errorFields.push(requiredFields[i]);
        }
    }
    var first_name = document.forms["contact_form"]["first_name"].value;
    if (first_name < 3)	{
        errorFields.push("first_name");
    }
    if (/[0-9]/.test(first_name)) {
        errorFields.push("first_name");
    }
    var last_name = document.forms["contact_form"]["last_name"].value;
    if (last_name < 3)	{
        errorFields.push("last_name");
    }
    if (/[0-9]/.test(last_name)) {
        errorFields.push("last_name");
    }
    var email = document.forms["contact_form"]["email"].value;
    if (!/.+?@.+?\..+/.test(email))	{
        errorFields.push("email");
    }
    var comment = document.forms["contact_form"]["comment"].value.length;
    if (comment < 50)	{
        errorFields.push("comment");
    }
    if (errorFields.length > 0)		{
        for (i = 0; i < errorFields.length; i++)	{
            var errorElement = document.getElementById(errorFields[i]);
            errorElement.className += " hasError";
        }
        var allErrorMessages = " ";
        for (i = 0; i < errorMessages.length; i++)	{
            allErrorMessages += errorMessages[i] + "<br/>";
        }
        return false;
    }	else	{
        return true;
    }
}
function checkEmpty(fieldValue)		{
    if (fieldValue == null || fieldValue == "" || fieldValue == "Choose a State")	{
        return true;
    } else	{
        return false;
    }
}
function upper(contact_form) {
    first_name.value = first_name.value.substr(0,1).toUpperCase() + first_name.value.substr(1);
    last_name.value = last_name.value.substr(0,1).toUpperCase() + last_name.value.substr(1);
    return false;
}
function firstNameError()		{
    document.getElementById("first_name").className = "form-control";
}
function lastNameError()		{
    document.getElementById("last_name").className = "form-control";
}
function emailError()		{
    document.getElementById("email").className = "form-control";
}
function commentError()		{
    document.getElementById("comment").className = "form-control";
}










// Resident ajax calls
$(document).on('click', '.utilities', function () {
    $.ajax({
        url: '/resident/resources/utilities',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.rent-buy', function () {
    $.ajax({
        url: '/resident/resources/rent-buy',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.landlords', function () {
    $.ajax({
        url: '/resident/resources/landlords',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.rentals', function () {
    $.ajax({
        url: '/resident/resources/rentals',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.credit', function () {
    $.ajax({
        url: '/resident/resources/credit',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});

//Client ajax calls
$(document).on('click', '.professionalism', function () {
    $.ajax({
        url: '/client/total-package/professionalism',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.placement', function () {
    $.ajax({
        url: '/client/total-package/placement',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.maintenance', function () {
    $.ajax({
        url: '/client/total-package/maintenance',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.fees-charges', function () {
    $.ajax({
        url: '/client/total-package/fees-charges',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.reporting', function () {
    $.ajax({
        url: '/client/total-package/reporting',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.other-services', function () {
    $.ajax({
        url: '/client/total-package/other-services',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.why-invest', function () {
    $.ajax({
        url: '/client/resources/why-invest',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.buying-process', function () {
    $.ajax({
        url: '/client/resources/buying-process',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.profit', function () {
    $.ajax({
        url: '/client/resources/profit',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
$(document).on('click', '.management', function () {
    $.ajax({
        url: '/client/resources/management',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});$(document).on('click', '.contact', function () {
    $.ajax({
        url: '/client/resources/contact',
        type: "GET", // not POST, laravel won't allow it
        success: function (data) {
            $data = $(data); // the HTML content your controller has produced
            $('#resources').fadeOut().html($data).fadeIn();
        }
    });
});
//# sourceMappingURL=all.js.map
