<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PublicController@index');
Route::get('/about', 'PublicController@about');
Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@validation');

Route::group(['prefix' => 'property'], function () {
    Route::get('/property-list', 'PublicController@properties');
    Route::get('/introduction', 'PublicController@rentIntroduction');
    Route::get('/tours', 'PublicController@tours');
    Route::get('/application', 'PublicController@application');
});

Route::group(['prefix' => 'resident'], function () {
    Route::get('/introduction', 'PublicController@residentIntroduction');
    Route::get('/handbook', 'PublicController@handbook');
    Route::get('/resources', 'PublicController@residentResources');
    Route::get('/faq', 'PublicController@residentFaq');
    //Ajax routes
    Route::get('/resources/utilities', 'AjaxController@utilities');
    Route::get('/resources/rent-buy', 'AjaxController@rentBuy');
    Route::get('/resources/landlords', 'AjaxController@landlords');
    Route::get('/resources/rentals', 'AjaxController@rentals');
    Route::get('/resources/credit', 'AjaxController@credit');
});

Route::group(['prefix' => 'client'], function () {
    Route::get('/introduction', 'PublicController@clientIntroduction');
    Route::get('/property-management', 'PublicController@management');
    Route::get('/total-package', 'PublicController@package');
    Route::get('/resources', 'PublicController@clientResources');
    Route::get('/faq', 'PublicController@clientFaq');
    //Ajax routes
    Route::get('/total-package/placement', 'AjaxController@placement');
    Route::get('/total-package/professionalism', 'AjaxController@professionalism');
    Route::get('/total-package/maintenance', 'AjaxController@maintenance');
    Route::get('/total-package/reporting', 'AjaxController@reporting');
    Route::get('/total-package/fees-charges', 'AjaxController@feesCharges');
    Route::get('/total-package/other-services', 'AjaxController@otherServices');
    Route::get('/resources/why-invest', 'AjaxController@whyInvest');
    Route::get('/resources/buying-process', 'AjaxController@buyingProcess');
    Route::get('/resources/profit', 'AjaxController@profit');
    Route::get('/resources/management', 'AjaxController@management');
    Route::get('/resources/contact', 'AjaxController@contact');
});

