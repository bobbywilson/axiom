<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
        'street',
        'city',
        'state',
        'zip',
        'beds',
        'baths',
        'square_feet',
        'rent',
        'fee',
        'deposit',
        'description',
        'name',
        'amenities',
        'promotions',
        'availability',
        'images'
    ];
}
