<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Support\Facades\DB;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $page = "Home";
        $home_status = 'class="active"';
        $about_status = 'class=""';
        $property_status = 'class=""';
        $resident_status = 'class=""';
        $client_status = 'class=""';
        $contact_status = 'class=""';

        return view('index', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    public function about() {
        $page = "About Us";
        $home_status = 'class=""';
        $about_status = 'class="active"';
        $property_status = 'class=""';
        $resident_status = 'class=""';
        $client_status = 'class=""';
        $contact_status = 'class=""';

        return view('public/about', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    //Property Views

    public function properties() {
        $page = "Rental Properties";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class="active"';
        $resident_status = 'class=""';
        $client_status = 'class=""';
        $contact_status = 'class=""';

        $property = DB::table('properties')->get();

        $images = json_decode($property[0]->images, true);
        $active_image = reset($images);

        return view('public/property/properties', compact('property', 'images', 'active_image', 'page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }
    public function rentIntroduction() {
        $page = "Rental Introduction";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class="active"';
        $resident_status = 'class=""';
        $client_status = 'class=""';
        $contact_status = 'class=""';

        return view('public/property/introduction', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    public function tours() {
        $page = "Rental Tours";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class="active"';
        $resident_status = 'class=""';
        $client_status = 'class=""';
        $contact_status = 'class=""';

        return view('public/property/tours', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    public function application() {
        $page = "Rental Application";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class="active"';
        $resident_status = 'class=""';
        $client_status = 'class=""';
        $contact_status = 'class=""';

        return view('public/property/application', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    //Client Views

    public function clientIntroduction() {
        $page = "Client Introduction";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class=""';
        $resident_status = 'class=""';
        $client_status = 'class="active"';
        $contact_status = 'class=""';

        return view('public/client/introduction', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    public function management() {
        $page = "Benefits of Property Management";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class=""';
        $resident_status = 'class=""';
        $client_status = 'class="active"';
        $contact_status = 'class=""';

        return view('public/client/management', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    public function package() {
        $page = "Total Real Estate Package";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class=""';
        $resident_status = 'class=""';
        $client_status = 'class="active"';
        $contact_status = 'class=""';

        return view('public/client/package', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    public function clientResources() {
        $page = "Client Resources";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class=""';
        $resident_status = 'class=""';
        $client_status = 'class="active"';
        $contact_status = 'class=""';

        return view('public/client/resources', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    public function clientFaq() {
        $page = "Client Facts and Questions";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class=""';
        $resident_status = 'class=""';
        $client_status = 'class="active"';
        $contact_status = 'class=""';

        return view('public/client/faq', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    //Resident Views

    public function residentIntroduction() {
        $page = "Resident Introduction";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class=""';
        $resident_status = 'class="active"';
        $client_status = 'class=""';
        $contact_status = 'class=""';

        return view('public/resident/introduction', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    public function handbook() {
        $page = "Resident Handbook";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class=""';
        $resident_status = 'class="active"';
        $client_status = 'class=""';
        $contact_status = 'class=""';

        return view('public/resident/handbook', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }

    public function residentResources() {
        $page = "Resident Introduction";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class=""';
        $resident_status = 'class="active"';
        $client_status = 'class=""';
        $contact_status = 'class=""';

        return view('public/resident/resources', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }
    public function residentFaq() {
        $page = "Resident Facts and Questions";
        $home_status = 'class=""';
        $about_status = 'class=""';
        $property_status = 'class=""';
        $resident_status = 'class="active"';
        $client_status = 'class=""';
        $contact_status = 'class=""';

        return view('public/resident/faq', compact('page', 'home_status', 'about_status', 'property_status', 'resident_status', 'client_status', 'contact_status'));
    }
}