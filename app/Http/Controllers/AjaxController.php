<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // Resident ajax views
    public function utilities() {
        return view('public/resident/ajax/utilities');
    }
    public function rentBuy() {
        return view('public/resident/ajax/rent-buy');
    }
    public function landlords() {
        return view('public/resident/ajax/landlords');
    }
    public function rentals() {
        return view('public/resident/ajax/rentals');
    }
    public function credit() {
        return view('public/resident/ajax/credit');
    }
    // Client ajax views
    public function placement() {
        return view('public/client/ajax/placement');
    }
    public function professionalism() {
        return view('public/client/ajax/professionalism');
    }
    public function maintenance() {
        return view('public/client/ajax/maintenance');
    }
    public function reporting() {
        return view('public/client/ajax/reporting');
    }
    public function feesCharges() {
        return view('public/client/ajax/fees-charges');
    }
    public function otherServices() {
        return view('public/client/ajax/other-services');
    }
    public function whyInvest() {
        return view('public/client/ajax/why-invest');
    }
    public function buyingProcess() {
        return view('public/client/ajax/buying-process');
    }
    public function profit() {
        return view('public/client/ajax/profit');
    }
    public function management() {
        return view('public/client/ajax/management');
    }
    public function contact() {
        return view('public/client/ajax/contact');
    }
}
